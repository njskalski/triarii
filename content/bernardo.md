---
title: Bernardo
subtitle: 
comments: false

---

![Bernard Gui being blessed by Pope John XXII](/Jean_XXII_Bernard_Gui.jpg)
source: [wikipedia](https://en.wikipedia.org/wiki/Bernard_Gui)

### Why

Every civilisation starts with a set of rules. Constraints that enable "innocent ignorance", sometimes called "separation of concerns". Assuming the constraints stand, one can limit their reasoning to a smaller world, where limited intellectual capacity will not stop them from making progress. A common example is road traffic - it is much easier to drive in countries, where rules of precedence are respected by other drivers.

### To civilize UX, I propose a following paradigm:

- windows/widgets form a tree. Root of that tree corresponds to entire screen, and each it's divided, a number of child nodes are introduced.
- parent node owns child nodes (in borrowchecker sense).
- each node is self contained. It is not possible to draw beyond borders of a node.
- focus ("where the input goes") always travels down in that tree. Any node can point at exactly ONE of it's descendant, or at none, in case it wants to consider the input itself. I call a path from some node down to a receiving node a *focus path*.
- the input is considered, but not always consumed. If a window does not use the input, it will be offered to it's immediate parent, and so on up to the root of the tree.
- the input is universal. All widgets interact with the same keyboard, mouse and user.
- messages, that are produced when input is consumed, are specialized, specific to a widget.
- a consumed input is translated into a message, first offered to a widget that did the translation.
- a widget updates through a single method called 'update'. All changes to widget state happen there. This does not imply however that a widget's apperance is determined only by this method - widget can rely on external sources of data (like filesystem) that can be updated between the draws, without calling the method 'update' of that widget explicitly.
- each 'update' call can result in creation of another message, offered first to immediate parent of widget. This way an event can create a cascade of escalation.

TODO

- at this point it has not been decided if 'message' offered to parent has to be of type specific to immediate parent (which would require re-packaging as it's escalated throughout the tree). The intuition "less options is better" says it should.

TODO

- this doesnt really belong here, but a common rule is: if you don't know which view should own 
the other view, consider "who is the next one in line to consume unused input?".

### Discovering the rules

A common shortcoming for most operating systems is lack of "focus stealing prevention", so far implemented only in XFCE (to my knowledge).
The way this shortcoming manifests itself is when a user types in one window *A* and another window *B* forces itself to foreground, capturing cursor and receiving some of the input meant for *A*. This is both source of anxiety and a security issue. *A* can be a password window and *B* can be plain-text. Or *A* can be a chat window and *B* can be some system dialog, with some options bound to letter keys - a fast typer might accidentally trigger a system update, reset or any other destructive operation.

There are however genuine situations where a cursor transfer makes sense: for instance, when one invokes "save as" dialog over editor window - here it makes sense to save user window navigation and immediately transfer focus to the dialog.

Another annoyance was found in IntelliJ - when "run configuration" window is opened, it blocks entire main window from receiving any input. If however some information necessary to create given "run configuration" happens to be say in "file tree" (for instance: location of file to be passed as run argument), user cannot access it without closing "run configuration" first. This limitation is clearly unnecessary, it's probably an unpredicted consequence of some other design decision that is too expensive to be changed anytime soon.

This leads to a rule: if parent window *A* becomes blocked by child window *B*, *A* points it's *focus path* to *B* and transfer of focus occurs.
At the same time, the child window cannot block any other windows than it's parent.
Furthermore, we aim at choosing "latest reasonable ancestor" to block, if any.
And lastly, we never allow *A* to be drawn anyway outside the boundaries of *B*, to prevent "obscuring receiving node".

### History

Before I started to work on Gladius, I had an earlier approach to a code editor. This approach was called [Sly Editor](github.com/njskalski/sly-editor). It failed, mostly because the [TUI library](https://github.com/gyscos/cursive) I used was built with a different paradigm in mind, one that was hard to merge with my goals. I managed to get some functionality, but then ended up in a state where adding any new features required major changes in both UI library and code editor. It became prohibitively expensive to move forward.

Since, I tried some other TUI libraries, like [tui](https://github.com/fdehau/tui-rs) or [tuirealm](https://github.com/veeso/tui-realm). I also digged into Elm, looking for some inspiration. All those experiments were published on github, but ultimately failed.

And then one day, some time around June 2021 I woke up and knew how to do it. It was not a result of deliberation in front of the whiteboard (I tried that), the problem was a little too wide to design it classicaly, and Rust is a little to strict to do it in "agile" way. 

A skeptic would say, that after so many attempts, I finally understood the problem well enough, to come up with genuinely new idea how to address it. But it felt different, like some kind of an englightment. I am not particularly religious, but I also don't think I was smart enough to come up with this on my own. If moments of inspiration happen, this was one of them.

It is an ancient tradition to start a literary piece with [invocation](https://en.wikipedia.org/wiki/Invocation) to a deity, asking for inspiration and guidance. However as a skeptic rediscovering Catholic culture that raised me, I recall [Mt 6 8](https://www.bibliacatolica.com.br/new-jerusalem-bible/matthew/6/) and don't formulate a plea. Instead, I say 

*thanks God for the nineties*.