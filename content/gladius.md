---
title: Gladius
subtitle: 
comments: false

---

![Gladius](/gladius_vertical.png)

### Etymology

Gladius (Latin: [ˈɡɫad̪iʊs̠]) is a Latin word meaning "sword" (of any type), but in its narrow sense, it refers to the sword of ancient Roman foot soldiers. [wikipedia](https://en.wikipedia.org/wiki/Gladius)

Gladius was not a Roman invention. One of key factors to success of Roman Empire was it's ability to assimilate best inventions of other cultures. Adopted from Hannibal's Celtiberian mercenaries, gladius was found superior to it's predecessor for it was excellent at both shashing and thrusting. [wikipedia](https://en.wikipedia.org/wiki/Gladius#Predecessors_and_origins).

### What

Gladius is a project of single-purpose, standard-issued, keyboard-only, command line integrated development environment.

One tool to rule them all. Fit for all purposes of daily programming job: reading and writing, code navigation, debugging.

Ready to use out-of-box, no dot-files and setup.

### Status

Currently pre-alpha: basic code editing and highlighting is implemented.

Gladius currently lives in Bernardo [repository](https://gitlab.com/njskalski/bernardo). It made no sense to separate it at this point, as it's a driver to Bernardo's design. I'm still in a process of discovering requirements of Bernardo given Gladius as long-term target.

### Why

In my life I mastered multiple "integrated programming environments". I started with Borland Delphi, later came Visual Studio, Netbeans, Eclipse, now IntelliJ. And frankly, I am tired. I am tired of switching and re-learning the most basic tool for my job. And since I expect to be programming for next 20-30 years, I guess it's justified to make my own IDE.

I tried adopting vim to my needs, but I got a feeling it's like old-timer car - it's more for people who enjoy spending weekends in their garage and ordering parts online. To me that's not a car, that's a hobby.

I want my car to 'work and fuck off',

and this is the mission statement of Gladius. 

Full operational capacity at nigh-zero maintenance. Setup - if any - disposable and easy to reproduce.

### How

Microsoft introduced [Language Server Protocol](https://microsoft.github.io/language-server-protocol/) and [Debug Adapter Protocol](https://microsoft.github.io/debug-adapter-protocol/), basically allowing me to outsource big part of the effort.
Another big piece comes from [Atom](https://atom.io/) project - I use [tree-sitter](https://tree-sitter.github.io/tree-sitter/) for parsing and highlighting.
I use [syntect](https://github.com/trishume/syntect) to read [TextMate](https://macromates.com/)'s tmTheme files.

Most IDEs create their "workspace" definitions. I will resort to use whatever given language offers and hope to come up with a definition that in majority of cases defaults to "most common scenario". This part of the design is still missing.

Lastly, I will enable certain "external plugins" at later date, like remote file-systems or code-search tools. These will work like LSP by communicating via pre-defined protocol on file-descriptors. The official distribution will contain example implementors of these features.

I do not expect to introduce any all-purpose scripting language. All key features are to be implemented directly in Rust and present in distributed binary, possibly disabled by default for lower-end hardware and constrained environments.

image source: [wikimedia](https://commons.wikimedia.org/wiki/File:Roman_gladius-transparent.png)