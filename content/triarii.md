---
title: Triarii
comments: false
---

# Triarii

### What (etymology)

**Triarii** (singular: Triarius) were one of the elements of the early Roman military manipular legions of the early Roman Republic (509 BC – 107 BC). They were the oldest and among the wealthiest men in the army and could afford high quality equipment. (...) [*wikipedia*](https://en.wikipedia.org/wiki/Triarii)


"Res ad triarios venit" which means “Things have come down to the Triarii” [*source*](https://imperiumromanum.pl/en/roman-army/units-of-roman-army/triarii/)

### Why

We live in times of "move quick, break things" and "houses built on the sand". As much as experiments and errors are necessary to advance research and development, it is only after designs mature that we can make another step forward. There is nothing experimental in a car radio.

The day is 24 hours long, and despite what our culture tells us, we need to sleep, eat and rest. In order to be able to solve new problems, we need to make old problems go away. There is plenty mature technologies around us. The rule of a thumb is that a technology is mature, when it does not require our attention any more. When was it the last time you needed to worry about electricity or water supply? Or what are the chances the food you buy in the shop will make you sick? These are problems that our civilisation (where present) solved successfully. We don't think about them, unless traveling to parts of the world that are still catching up.

Wifi on the other hand breaks all the time. You can certainly recall "restarting your router" at least once this year. I can also bet your printer is not working. Printers are the final bosses of our daily fight against [entropy](https://en.wikipedia.org/wiki/Entropy). They break more often than we use them. 

We're not going to do any meaningful work if we keep wasting our time on fixing printers. 

### Mission

This website is dedicated to idea of maturing the design. In my spare time I want to write software that requires zero maintenance. That people can rely on in the direst of situations. I want to author technologies that are worth mastering, because they will be around in a decade.

### Status and limitations

At this point I work on a project I called "Gladius", which required me to write a supporting TUI library I call "Bernardo". **These are not ready for release yet**, but it was necessary to publish this website in order to attract open source contribution. 

### Why now

As [Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman) properly predicted, unless we are in control of source code, the machine is not working for us. The reason why the printer doesn't work, car radio works with a smartphone only occasionally and you can't uninstall stock browser, is because the companies that manufacture these products are not interested in your agenda. They are interested in their profits. And they will pursue them at the expense of your agenda as far as they can: until some form of resistance hurts their profits.

The printer is designed to sell you ink, the car radio is designed to sell you map subscription, the phone is about monetizing your natural human desire to connect with other people. And these parts of their respective designs receive most serious investment.

And increasingly the game is not about your money, it is about your *attention*. 

See, what they have realized is that people guard their money pretty well, but don't protect their attention at all. They got really good in "aggregating the eyeballs" and selling our attention in bulk. 

Each "free website" bombards us with ads and wants to send us notifications. It's impossible to order a physical book online without giving away your phone number. It is "normal" to receive 20 letters over 5 years from a car manufacturer after making a single purchase.

Entire world wide web has been hijacked by marketing money, with last sanctuaries of structured thought like Wikipedia and rebielions of free software loosing their ground to addictive popculture daily.

I miss the "old days" when I could have disconnected myself from internet and in a peace of library, where phone chimes were frown upon, have a day of uninterrupted thought and learn something tangible.

I miss the constraints of limited hardware, uncapable of screaming at me with animated ads. I miss unstable and expensive internet connection that forced the communication to be concise and efficient. I miss software being burned on a CD and delivered by mail, making "patching" prohibitively expensive.

To this day in order to focus, I retreat to a sanctuary of dulled colors and disconnected spaces, on the outskirts of what became of our civilisation.

I struggled a daily fight against websites of mass distraction for too long.

In order to reassert the initiative, I have to fall back to safety of proven ways.

I retreat to the command line,

where "it comes down to the Triarii".