---
title: About me
subtitle: 
comments: false

---

My name is Andrzej Janusz Skalski, I go by the name NJ, which phonetically sounds similarly to my first name.  

I am a coder.

Born in Warsaw, Poland, living on the outskirts of Zürich, Switzerland since 2016.

I like coding, gym, surfing and dogs.
I dislike meetings, www, printers and doing laundry.
